//
//  ExperienceTableViewCell.swift
//  CV
//
//  Created by Max Bekkhus on 2018-12-02.
//  Copyright © 2018 Max Bekkhus. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var experienceImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var timeInterval: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
