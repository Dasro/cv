//
//  Experience.swift
//  CV
//
//  Created by Max Bekkhus on 2018-12-02.
//  Copyright © 2018 Max Bekkhus. All rights reserved.
//

import Foundation

struct Experience {
    
    var imageName: String
    var title: String
    var timeInterval: String
    var description: String
    
}
