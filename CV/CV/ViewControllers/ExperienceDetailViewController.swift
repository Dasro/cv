//
//  ExperienceDetailViewController.swift
//  CV
//
//  Created by Max Bekkhus on 2018-12-02.
//  Copyright © 2018 Max Bekkhus. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
    
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var timeInterval: UILabel!
    @IBOutlet weak var detailDescription: UILabel!
    
    var experience: Experience = Experience(imageName: "", title: "", timeInterval: "", description: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailImageView.image = UIImage(named: experience.imageName)
        detailTitle.text = experience.title
        self.title = experience.title
        timeInterval.text = experience.timeInterval
        detailDescription.text = experience.description
    }
    
    
}
