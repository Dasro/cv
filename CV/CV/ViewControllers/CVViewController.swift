//
//  ViewController.swift
//  CV
//
//  Created by Max Bekkhus on 2018-12-02.
//  Copyright © 2018 Max Bekkhus. All rights reserved.
//

import UIKit

class CVViewController: UIViewController {

    @IBOutlet weak var cvImageView: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var zipCodeAndCityLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var personalPresentation: UILabel!
    @IBOutlet weak var experienceButton: UIButton!
    @IBOutlet weak var skillsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvImageView.image = UIImage(named: "google")
        phoneNumberLabel.text = "0703126939"
        addressLabel.text = "Lustigkullegatan 12"
        zipCodeAndCityLabel.text = "55453 Jönköping"
        personalPresentation.lineBreakMode = .byWordWrapping
        personalPresentation.layer.cornerRadius = 10
        personalPresentation.text = "Lorem ipsum dolor sit amet, eos in elitr nostro, quis constituto eu vim. At usu utinam epicurei mnesarchum, nam tamquam inimicus eu. Vel at definiebas honestatis, vis inermis lucilius cu. At mei aperiam accusam. Cu solet detraxit perfecto has, an eum case veri scribentur, modo saepe in sit. Cu nibh brute altera has, ad quo probo fuisset deserunt, nusquam omittantur consectetuer eu sea. Vocent viderer ut cum. Te his feugait propriae phaedrum, est iusto eirmod persequeris ut. Soleat platonem posidonium duo ei, est suavitate disputando no. Singulis pertinax te mel."
        
        // Do any additional setup after loading the view, typically from a nib.
    }


}

