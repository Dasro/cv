//
//  ExperienceViewController.swift
//  CV
//
//  Created by Max Bekkhus on 2018-12-02.
//  Copyright © 2018 Max Bekkhus. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var workExperiences: [Experience] = []
    var educationExperiences: [Experience] = []
    var experiences: [[Experience]] = []
    
    var sectionNames: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = UIColor.clear
        
        workExperiences = [
            Experience(imageName: "facebook", title: "Senior Beginner", timeInterval: "2011-2013", description: "The security vulnerability trigger"),
            Experience(imageName: "google", title: "Python Guru", timeInterval: "2014-2016", description: "Pythonian Interpreter")
        ]
        
        educationExperiences = [
            Experience(imageName: "jth", title: "Tekniskt Basår", timeInterval: "2016-2017", description: "Maths, physics and chemistry"),
            Experience(imageName: "jth", title: "Datateknik", timeInterval: "2017-On going", description: "Lots of code")
        ]
        
        experiences = [workExperiences, educationExperiences]
        
        sectionNames = ["Work Experience", "Education"]
        
        tableView.tableFooterView = UIView()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let experience = experiences[indexPath.section][indexPath.row]
                destination.experience = experience
            }
        }
    }
    
}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell {
            
            let experience = experiences[indexPath.section][indexPath.row]
            cell.experienceImageView.image = UIImage(named:experience.imageName)
            cell.title.text = experience.title
            cell.timeInterval.text = experience.timeInterval
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor.black
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
        headerLabel.text = sectionNames[section]
        headerLabel.textColor = UIColor.white
        headerLabel.font = UIFont.boldSystemFont(ofSize: 15)
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "DetailSegue", sender: indexPath)
    }
    
}
